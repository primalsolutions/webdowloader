﻿namespace WebDownloader.Models
{
    public class Page
    {
        public string Url { get; set; }
        public PageStatus Status { get; set; }
        public bool IndexPage { get; set; }

        public Page(string url)
        {
            Url = url;
            Status = PageStatus.Initialized;
        }
    }
}
