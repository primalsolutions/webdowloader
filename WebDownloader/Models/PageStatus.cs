﻿namespace WebDownloader.Models
{
    public enum PageStatus
    {
        Initialized = 0,
        Downloading = 1,
        Completed = 2
    }
}