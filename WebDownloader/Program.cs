﻿using System;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using RestSharp;
using ShellProgressBar;
using WebDownloader.Downloading;
using WebDownloader.Processing;

namespace WebDownloader
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var serviceCollection = new ServiceCollection();
            ConfigureServices(serviceCollection);
            var serviceProvider = serviceCollection.BuildServiceProvider();

            var downloader = serviceProvider.GetRequiredService<IDownloader>();

            Task.Run(() => downloader.Run(@"https://tretton37.com", 5)).Wait();
        }

        private static void ConfigureServices(IServiceCollection services)
        {
            services.AddLogging(configure => configure.AddConsole().AddDebug());

            services.AddTransient<IDownloader, Downloader>();
            services.AddSingleton<ILinkExtractor, AgilityLinkExtractor>();

            services.AddHttpClient();
        }
    }
}