﻿using ShellProgressBar;
using System;

namespace WebDownloader.Processing
{
    public static class Reporter
    {
        public static ProgressBar pbar = new ProgressBar(10000, "", new ProgressBarOptions
        {
            ForegroundColor = ConsoleColor.Yellow,
            ForegroundColorDone = ConsoleColor.DarkGreen,
            BackgroundColor = ConsoleColor.DarkGray,
            BackgroundCharacter = '\u2593'
        });

        public static IProgress<double> progress = pbar.AsProgress<double>();

        internal static void UpdateProgress(double v)
        {
            progress.Report(v);
        }

        internal static void UpdateUrl(string url)
        {
            pbar.Message = $"[{Thread.CurrentThread.ManagedThreadId}]: {url}";
        }
    }
}
