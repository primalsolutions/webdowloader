﻿using System.Collections.Concurrent;
using WebDownloader.Models;

namespace WebDownloader.Processing
{
    public interface ILinkExtractor
    {
        public IEnumerable<Page> ExtractLinks(ConcurrentDictionary<string, bool> processedSites, string baseUrl, string fileName);
    }
}
