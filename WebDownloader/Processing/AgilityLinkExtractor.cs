﻿using HtmlAgilityPack;
using System.Collections.Concurrent;
using System.Text.RegularExpressions;
using WebDownloader.Models;

namespace WebDownloader.Processing
{
    public class AgilityLinkExtractor : ILinkExtractor
    {
        private readonly Regex _sectionFilter = new Regex(@"#.*$");
        public IEnumerable<Page> ExtractLinks(ConcurrentDictionary<string, bool> processedSites, string baseUrl, string fileName)
        {
            var doc = new HtmlDocument();
            doc.Load(fileName);

            var linkedPages = doc.DocumentNode.Descendants("a")
                                  .Select(a => a.GetAttributeValue("href", string.Empty))
                                  .Select(lp => _sectionFilter.Replace(lp, ""))
                                  .Where(lp => !(string.IsNullOrWhiteSpace(lp) || lp.Equals("/")));

            string newUrl;
            foreach (var linkedPage in linkedPages)
            {
                if (linkedPage.StartsWith("/"))
                {
                    newUrl = baseUrl + linkedPage;
                    if (processedSites.TryAdd(newUrl, false)) {
                        yield return new Page(newUrl);
                    }
                }
                if (linkedPage.StartsWith(baseUrl))
                {
                    if (processedSites.TryAdd(linkedPage, false))
                    {
                        yield return new Page(linkedPage);
                    }
                }
            }
        }
    }
}
