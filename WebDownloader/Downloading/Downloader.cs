﻿using Microsoft.Extensions.Logging;
using System.Collections.Concurrent;
using WebDownloader.Models;
using WebDownloader.Processing;

namespace WebDownloader.Downloading
{
    public class Downloader : IDownloader
    {
        private readonly string FOLDERNAME = "Downloaded Websites";
        

        private readonly ILogger _logger;
        private readonly ILinkExtractor _linkExtractor;
        private readonly HttpClient _httpClient;

        private readonly ConcurrentDictionary<string, bool> _discoveredSites;
        private readonly ConcurrentQueue<Page> _processingQueue;
        private string _baseUrl;
        

        public Downloader(ILogger<Downloader> logger, HttpClient httpClient, ILinkExtractor linkExtractor)
        {
            _logger = logger;
            _linkExtractor = linkExtractor;
            _httpClient = new HttpClient();

            _discoveredSites = new ConcurrentDictionary<string, bool>();
            _processingQueue = new ConcurrentQueue<Page>();
        }

        public async Task Run(string baseUrl, int threads)
        {
            _processingQueue.Enqueue(new Page(baseUrl) { IndexPage = true });
            _baseUrl = baseUrl;

            Directory.CreateDirectory(FOLDERNAME);

            while (_processingQueue.Count > 0)
            {
                var tasks = new List<Task>();
                for (int i = 0; i < threads; i++)
                {
                    tasks.Add(Task.Run(() => ProcessUrl()));
                }

                await Task.WhenAll(tasks);
            }
        }

        public async Task ProcessUrl()
        {
            if (!_processingQueue.TryDequeue(out Page currPage))
            {
                return;
            }

            currPage.Status = PageStatus.Downloading;

            var fileName = Path.Combine(FOLDERNAME, UrlToFilename(currPage));
            var path = Path.GetDirectoryName(fileName);

            Directory.CreateDirectory(path);

            var response = await _httpClient.GetAsync(currPage.Url);

            if(!response.IsSuccessStatusCode)
            {
                _logger.LogInformation($"Failed to retrieve: {currPage.Url}");
                return;
            }

            Reporter.UpdateUrl(currPage.Url);
            using (var fs = new FileStream(fileName, FileMode.Create))
            {
                await response.Content.CopyToAsync(fs);
            }

            _discoveredSites.AddOrUpdate(currPage.Url, true, (_, _) => true);

            var newLinks = _linkExtractor.ExtractLinks(_discoveredSites, _baseUrl, fileName);

            foreach (var link in newLinks)
            {
                _processingQueue.Enqueue(link);
            }

            Reporter.UpdateProgress(_discoveredSites.Count(kvp => kvp.Value) / (_discoveredSites.Count() + (double)_processingQueue.Count()));
        }

        public string UrlToFilename(Page page)
        {
            var ret = page.Url.Replace("://", "_").Replace(".", "_");

            ret += page.IndexPage ? "/index.html" : ".html";

            return ret;
        }
    }
}
