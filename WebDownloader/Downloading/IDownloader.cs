﻿namespace WebDownloader.Downloading
{
    public interface IDownloader
    {
        public Task Run(string baseUrl, int threads);
    }
}
